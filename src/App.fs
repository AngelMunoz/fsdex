module App

open System

open Elmish
open Feliz
open Feliz.Router
open Feliz.UseElmish

open type Feliz.Html
open type Feliz.prop

open GlobalDex
open PokedexByGen

[<RequireQualifiedAccess>]
type Page =
    | GlobalDex
    | DexByGen of int option
    | NotFound

type State = { page: Page }
type Msg = SetPage of Page

let parseUrl =
    function
    | [] -> Page.GlobalDex
    | [ "pokedex" ] -> Page.DexByGen None
    | [ "pokedex"; Route.Query [ "gen", Route.Int gen ] ] -> Page.DexByGen(Some gen)
    | _ -> Page.NotFound

let init () =
    { page = (Router.currentUrl () |> parseUrl) }, Cmd.none

let update (msg: Msg) (state: State) =
    match msg with
    | SetPage page -> { state with page = page }, Cmd.none


[<ReactComponent>]
let Pokedex () =
    let (state, dispatch) = React.useElmish (init, update, [||])

    let currentPage =
        match state.page with
        | Page.GlobalDex -> GlobalDexPage()
        | Page.DexByGen gen -> PokedexByGen({| gen = gen |})
        | Page.NotFound -> h1 "Not Found"

    article [
        children [
            nav [ children [] ]
            main [
                children [
                    React.router [
                        router.onUrlChanged (parseUrl >> SetPage >> dispatch)
                        router.children currentPage
                    ]
                ]
            ]
        ]
    ]

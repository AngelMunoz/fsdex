module GlobalDex

open Feliz
open Feliz.UseElmish
open Feliz.Router
open Elmish

open type Feliz.Html
open type Feliz.prop

open Types
open System

type private State =
    { canGoNext: bool
      canGoBack: bool
      page: int
      results: PaginatedResult<NamedAPIResource> option }

type private Msg =
    | GoNext
    | GoBack
    | RequestFailed of {| reason: string; setPage: int |}
    | SetResult of PaginatedResult<NamedAPIResource>


let private init (): State * Cmd<Msg> =
    { page = 1
      canGoNext = true
      canGoBack = false
      results = None },
    Cmd.OfPromise.perform Pokemon.GetPokemon None SetResult

let private update (msg: Msg) (state: State): State * Cmd<Msg> =
    match msg with
    | GoNext ->
        { state with
              page = state.page + 1
              canGoBack = false
              canGoNext = false },
        Cmd.OfPromise.either
            Pokemon.GetPokemon
            state.results.Value.next
            SetResult
            (fun err ->
                RequestFailed
                    {| reason = err.Message
                       setPage = state.page |})
    | GoBack ->
        { state with
              page = state.page - 1
              canGoBack = false
              canGoNext = false },
        Cmd.OfPromise.either
            Pokemon.GetPokemon
            state.results.Value.previous
            SetResult
            (fun err ->
                RequestFailed
                    {| reason = err.Message
                       setPage = state.page |})
    | RequestFailed payload ->
        printfn "%s" payload.reason
        { state with page = payload.setPage }, Cmd.none
    | SetResult pagination ->

        let (canGoNext, canGoBack) =
            match pagination.next, pagination.previous with
            | Some _, Some _ -> (true, true)
            | None, Some _ -> (false, true)
            | Some _, None -> (true, false)
            | None, None -> (false, false)

        { state with
              results = Some pagination
              canGoBack = canGoBack
              canGoNext = canGoNext },
        Cmd.none

[<ReactComponent>]
let GlobalDexPage () =
    let (state, dispatch) = React.useElmish (init, update, [||])

    let pokemon, pages =
        match state.results with
        | Some results ->
            results.results,
            (Math.Ceiling((results.count |> float) / 20.0)
             |> int)
        | None -> [], 0


    article [
        header [
            text "All Pokemon (not in dex order)"
        ]
        nav [
            children [
                section [
                    children [
                        button [
                            text "Previous"
                            disabled (not state.canGoBack)
                            onClick (fun _ -> dispatch GoBack)
                        ]
                        label [
                            text $"Page %i{state.page} of %i{pages}"
                        ]
                        button [
                            text "Next"
                            disabled (not state.canGoNext)
                            onClick (fun _ -> dispatch GoNext)
                        ]
                    ]
                ]
                section [
                    children [
                        a [
                            href (Router.format "pokedex")
                            text "Pokedex by generation"
                        ]
                    ]
                ]

            ]

        ]

        section [
            ul [
                children [
                    for pokemon in pokemon do
                        li [
                            children [
                                b pokemon.name
                                small [ a [ text "Details" ] ]
                            ]
                        ]
                ]
            ]
        ]

    ]

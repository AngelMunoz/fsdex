module PokedexByGen

open Elmish
open Feliz
open Feliz.UseElmish

open type Feliz.Html
open type Feliz.prop

open Types

type private State =
    { generation: int option
      pokedex: Pokedex option
      results: PaginatedResult<NamedAPIResource> option }

type private Msg = SetPokedex of ResponseType<Pokedex>

let private init (generation: int option) =
    fun () ->
        { generation = generation
          pokedex = None
          results = None },
        Cmd.OfPromise.perform Pokemon.GetPokedex generation SetPokedex

let private update (msg: Msg) (state: State): State * Cmd<Msg> =
    match msg with
    | SetPokedex pokedex ->
        let state =
            match pokedex with
            | Details pokedex -> { state with pokedex = Some pokedex }
            | Paginated results -> { state with results = Some results }

        state, Cmd.none


[<ReactComponent>]
let PokedexByGen (props: {| gen: int option |}) =
    let state, dispatch =
        React.useElmish (init props.gen, update, [||])

    let pokedexes =
        match state.results with
        | Some results -> results.results
        | None -> []

    article [
        header [
            text $"Generation: %A{state.generation}"
        ]
        nav [
            children [
                for entry in pokedexes do
                    li []
            ]
        ]
    ]

[<RequireQualifiedAccess>]
module Pokemon

open Fable.Core
open Thoth.Fetch
open Thoth.Json
open Types


let private baseUrl =
    Config.variableOrDefault "POKEMON_API_URL" "https://pokeapi.co/api/v2"

let GetPokemon (url: string option): JS.Promise<PaginatedResult<NamedAPIResource>> =
    let url = defaultArg url $"{baseUrl}/pokemon"
    Fetch.get (url, caseStrategy = SnakeCase)

let GetPokedex (generation: int option): JS.Promise<ResponseType<Pokedex>> =
    match generation with
    | Some gen ->
        promise {
            let! result = Fetch.get ($"%s{baseUrl}/pokedex/%i{gen}", caseStrategy = SnakeCase)
            return Details result
        }
    | _ ->
        promise {
            let! result = Fetch.get ($"%s{baseUrl}/pokedex?offset=0&limit=100", caseStrategy = SnakeCase)
            return Paginated result
        }

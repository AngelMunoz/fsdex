module Types

type NamedAPIResource = { name: string; url: string }

type Description =
    { description: string
      language: NamedAPIResource }

type Name =
    { name: string
      language: NamedAPIResource }

type PokemonEntry =
    { entry_number: int
      pokemon_species: NamedAPIResource }

type PokemonAbility =
    { is_hidden: bool
      slot: int
      ability: NamedAPIResource }

type VersionGameIndex =
    { game_index: int
      version: NamedAPIResource }

type PokemonHeldItemVersion =
    { version: NamedAPIResource
      rarity: int }

type PokemonHeldItem =
    { item: NamedAPIResource
      version_details: PokemonHeldItemVersion list }

type PokemonMoveVersion =
    { move_learn_method: NamedAPIResource
      version_group: NamedAPIResource
      level_learned_at: int }

type PokemonMove =
    { move: NamedAPIResource
      version_group_details: PokemonMoveVersion list }

type PokemonSprites =
    { front_default: string
      front_shiny: string
      front_female: string
      front_shiny_female: string
      back_default: string
      back_shiny: string
      back_female: string
      back_shiny_female: string }

type PokemonStat =
    { state: NamedAPIResource
      effort: int
      base_stat: int }

type PokemonType =
    { slot: int
      ``type``: NamedAPIResource }

type Pokedex =
    { id: int
      name: string
      is_main_series: bool
      descriptions: Description list
      names: Name list
      pokemon_entries: PokemonEntry list
      region: NamedAPIResource
      version_groups: NamedAPIResource }

type Pokemon =
    { id: int
      name: string
      base_experience: int
      height: int
      is_default: bool
      order: int
      weight: int
      abilities: PokemonAbility list
      forms: NamedAPIResource list
      game_indices: VersionGameIndex list
      held_items: PokemonHeldItem list
      location_area_encounters: string
      moves: PokemonMove list
      sprites: PokemonSprites
      species: NamedAPIResource
      stats: PokemonStat list
      types: PokemonType list }



// Custom Types

type RegionName =
    | National
    | Kanto
    | OriginalJohto
    | Hoenn
    | OriginalSinnoh
    | ExtendedSinnoh
    | UpdatedJohto
    | OriginalUnova
    | UpdatedUnova
    | ConquestGallery
    | KalosCentral
    | KalosCoastal
    | KalosMountain
    | UpdatedHoenn
    | OriginalAlola
    | OriginalMelemele
    | OriginalAkala
    | OriginalUlaula
    | OriginalPoni
    | UpdatedAlola
    | UpdatedMelemele
    | UpdatedAkala
    | UpdatedUlaula
    | UpdatedPoni
    | UpdatedKanto
    | Galar
    | IsleOfArmor
    | CrownTundra

    member x.GetId() =
        match x with
        | National -> 1
        | Kanto -> 2
        | OriginalJohto -> 3
        | Hoenn -> 4
        | OriginalSinnoh -> 5
        | ExtendedSinnoh -> 6
        | UpdatedJohto -> 7
        | OriginalUnova -> 8
        | UpdatedUnova -> 9
        | ConquestGallery -> 11
        | KalosCentral -> 12
        | KalosCoastal -> 13
        | KalosMountain -> 14
        | UpdatedHoenn -> 15
        | OriginalAlola -> 16
        | OriginalMelemele -> 17
        | OriginalAkala -> 18
        | OriginalUlaula -> 19
        | OriginalPoni -> 20
        | UpdatedAlola -> 21
        | UpdatedMelemele -> 22
        | UpdatedAkala -> 23
        | UpdatedUlaula -> 24
        | UpdatedPoni -> 25
        | UpdatedKanto -> 26
        | Galar -> 27
        | IsleOfArmor -> 28
        | CrownTundra -> 29

    static member OfString(name: string) =
        match name.ToLowerInvariant() with
        | "national" -> National
        | "kanto" -> Kanto
        | "original-johto" -> OriginalJohto
        | "hoenn" -> Hoenn
        | "original-sinnoh" -> OriginalSinnoh
        | "extended-sinnoh" -> ExtendedSinnoh
        | "updated-johto" -> UpdatedJohto
        | "original-unova" -> OriginalUnova
        | "updated-unova" -> UpdatedUnova
        | "conquest-gallery" -> ConquestGallery
        | "kalos-central" -> KalosCentral
        | "kalos-coastal" -> KalosCoastal
        | "kalos-mountain" -> KalosMountain
        | "updated-Hoenn" -> UpdatedHoenn
        | "original-alola" -> OriginalAlola
        | "original-melemele" -> OriginalMelemele
        | "original-akala" -> OriginalAkala
        | "original-ulaula" -> OriginalUlaula
        | "original-poni" -> OriginalPoni
        | "updated-alola" -> UpdatedAlola
        | "updated-melemele" -> UpdatedMelemele
        | "updated-akala" -> UpdatedAkala
        | "updated-ulaula" -> UpdatedUlaula
        | "updated-poni" -> UpdatedPoni
        | "updated-kanto" -> UpdatedKanto
        | "galar" -> Galar
        | "isle-of-armor" -> IsleOfArmor
        | "crown-tundra" -> CrownTundra
        | _ -> failwith "Unknown value"



type PaginatedResult<'T> =
    { count: int
      next: string option
      previous: string option
      results: 'T list }

type ResponseType<'T> =
    | Details of 'T
    | Paginated of PaginatedResult<NamedAPIResource>
